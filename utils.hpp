#include <iostream>
#include <vector>
#include <thread>
#include <cmath>
#include <map>
#include <random>
#include <fstream>
#include <semaphore.h>

using namespace std;

extern sem_t emissionLock;
extern int totalEmission;

struct logStruct
{
    std::chrono::milliseconds entranceTime;
    std::chrono::milliseconds exitTime;
    int emission;
};

typedef map<string, int> PathMap;
typedef struct logStruct Log;

int generateRandom (int base);
int calcPolution (int p, int h);
string itos(int i);
void initSem();

class Car;
