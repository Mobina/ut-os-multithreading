#include "utils.hpp"

#ifndef STREET_H
#define STREET_H

class Street 
{
private:
    int hardness;
    char startNode;
    char endNode;
    // pthread_mutex_t monitor = PTHREAD_MUTEX_INITIALIZER;
    sem_t monitor;
    
public:
    Street (char s, char e, int h);
    string getNodes ();
    Log enter (int p);
};

#endif