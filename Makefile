all.o: main.o car.o street.o utils.o
	g++ -std=c++11 main.o car.o street.o utils.o -o main.out -lpthread

main.o: main.cpp car.hpp car.cpp
	g++ -std=c++11 -c main.cpp car.hpp car.cpp
 
car.o: car.hpp car.cpp street.hpp street.cpp
	g++ -std=c++11 -c car.hpp car.cpp street.hpp street.cpp

street.o: street.hpp street.cpp utils.hpp utils.cpp
	g++ -std=c++11 -c street.hpp street.cpp utils.hpp utils.cpp

utils.o: utils.hpp utils.cpp
	g++ -std=c++11 -c utils.hpp utils.cpp

clean:
	rm -rf *.o *.hpp.gch