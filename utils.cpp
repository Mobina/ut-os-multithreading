#include "utils.hpp"

sem_t emissionLock;
int totalEmission = 0;

int generateRandom (int base)
{
    int randomNumber;

    random_device r;
    seed_seq seed{r(), r(), r(), r(), r(), r(), r(), r()};
    mt19937 eng{seed};
    uniform_int_distribution<> dist(1,base);
    randomNumber = dist(eng);
    return randomNumber;
}

int calcPolution (int p, int h)
{
    int sum = 0;
    for (int k = 0; k <= 10000000; k++)
    {
        float fraction = k / (1000000 * p * h);
        sum += floor(fraction);
    }
    return sum;
}

string itos(int i)
{
    string s = "";
	if (i == 0)
		s = char(48) + s;
    while (i > 0) 
    {
        s = char(i % 10 + '0') + s;
        i = i / 10;
    }
    return s;
}

void initSem()
{
    sem_init(&emissionLock, 0, 1);
}