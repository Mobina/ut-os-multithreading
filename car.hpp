#include "street.hpp"
#ifndef CAR_H
#define CAR_H


class Car
{
private:
    int polution;
    int pathNum;
    int carNum;

public:
    Car(int p, int pathnum, int carnum);
    int getPolution();
    void move (vector<Street*> path);
};

#endif