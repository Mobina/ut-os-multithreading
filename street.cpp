#include "street.hpp"

Street::Street (char s, char e, int h)
{
    startNode = s;
    endNode = e;
    hardness = h;
    sem_init(&monitor, 0, 1);
}

string Street::getNodes ()
{
    string nodes;
    nodes += startNode;
    nodes += endNode;
    return nodes;
}

Log Street::enter (int p)
{
    // pthread_mutex_lock(&monitor);
    sem_wait(&monitor);
    Log log;
    std::chrono::milliseconds ms = std::chrono::duration_cast< std::chrono::milliseconds >(
        std::chrono::system_clock::now().time_since_epoch()
    );
    log.entranceTime = ms;
    int sum = calcPolution(p, hardness);
    log.emission = sum;
    ms = std::chrono::duration_cast< std::chrono::milliseconds >(
        std::chrono::system_clock::now().time_since_epoch()
    );
    log.exitTime = ms;
    // pthread_mutex_unlock(&monitor);
    sem_post(&monitor);
    return log;
}