#include "car.hpp"

int main(int argc, char* argv[])
{
    if (argc < 2) 
    {
        cerr << "usage: <input file name>\n";
        return 1;
    }
    char* filename = argv[1];
    vector<Street> streets;
    initSem();
    string str;
    ifstream file;
    file.open(filename, ios::in);
    while (getline(file, str))
    {
        if (str == "#")
            break;
        vector<char> curStreet;
        for (int j = 0; j < str.size(); j++)
        {
            if (str[j] == ' '
                || str[j] == '-')
                continue;
            if (curStreet.size() < 2)
                curStreet.push_back(str[j]);
            else
            {
                int h = int(str[j]) - 48;
                Street newStreet = Street(curStreet[0], curStreet[1], h);
                streets.push_back(newStreet);
            }
            
        }
    }
    bool isValue = false;
    PathMap pathMap;
    string curPath;
    while (getline(file, str))
    {
        if (str.size() < 1)
            break;
        if (isValue)
        {
            pathMap.insert(pair<string, int>(curPath, atoi(str.c_str()))); 
            isValue = false;
            curPath = "";
        }
        else
        {
            for (int j = 0; j < str.size(); j++)
            {
                if (str[j] == ' '
                    || str[j] == '-')
                    continue;
                curPath += str[j];
            }
            isValue = true;
        }
    }
    file.close();
    vector<thread> threads;
    PathMap::iterator itr;
    int pathNum = 0;
    for (itr = pathMap.begin(); itr != pathMap.end(); itr++) 
    {
        string nodes;
        vector<Street*> path;
        int count = 0;
        for (int i = 0; i < (itr->first).size() + 1; i++)
        {
            if (count < 2)
            {
                nodes += (itr->first)[i];
                count++;
            }
            else
            {
                count = 0;
                for (int k = 0; k < streets.size(); k++)
                {
                    if (streets[k].getNodes() == nodes)
                    {
                        path.push_back(&streets[k]);
                        nodes = "";
                    }
                }
                i = i - 2;
            }
        }
        for (int j = 0; j < itr->second; j++)
        {
            Car car = Car(generateRandom(10), pathNum, j);
            threads.emplace_back(thread(&Car::move, &car, path));
        }
        pathNum++;
    } 

    for (int i = 0; i < threads.size(); i++)
        if (threads[i].joinable())
            threads[i].join();
}