#include "car.hpp"

Car::Car(int p, int pathnum, int carnum)
{
    polution = p;
    pathNum = pathnum;
    carNum = carnum;
}

int Car::getPolution()
{
    return polution;
}

void Car::move (vector<Street*> path)
{
    ofstream file;
    string filename = itos(pathNum) + itos(carNum);
    file.open(filename, ios::out);
    for (int i = 0; i < path.size(); i++) 
    {
        Log log = path[i]->enter(polution);
        sem_wait(&emissionLock);
        totalEmission += log.emission;
        sem_post(&emissionLock);
        file << (path[i]->getNodes())[0] << ','
            << log.entranceTime.count() << ','
            << (path[i]->getNodes())[1] << ','
            << log.exitTime.count() << ','
            << log.emission << ','
            << totalEmission << '\n';
    }
    file.close();
}

